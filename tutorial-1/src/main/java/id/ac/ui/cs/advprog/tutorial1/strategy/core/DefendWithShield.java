package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    @Override
    public String defend() {
        return "defend with shield";
    }

    @Override
    public String getType() {
        return "Shield";
    }
}
