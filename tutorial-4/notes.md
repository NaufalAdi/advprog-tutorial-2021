Pada lazy instantiation objek singleton, objek baru diinstansiasi ketika pertama dipanggil method getInstance.
Keuntungan dari lazy instantiation adalah ketika objek singleton cukup mahal, tidak memenuhi memory karena hanya dibuat 
jika dibutuhkan. Tetapi lazy instantiation sulit menangani multithreading karena jika method getinstance dipanggil oleh
beberapa thread di waktu bersamaan ada kemungkinan beberapa instance terbuat.

Pada eager instantiation, objek singleton diinstansiasi dari awal runtime. Multithreading tidak menjadi masalah karena
objek sudah diinstansiasi dari awal. Tetapi karena diinstansiasi dari awal jadi lebih berat meskipun belum digunakan.