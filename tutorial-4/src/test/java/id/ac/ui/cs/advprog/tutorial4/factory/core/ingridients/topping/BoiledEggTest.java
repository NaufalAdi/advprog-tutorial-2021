package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

//add tests
public class BoiledEggTest {
    private Class<?> boiledEggClass;
    private BoiledEgg boiledEgg;

    @BeforeEach
    public void setUp() throws Exception {
        boiledEggClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg");
        boiledEgg = new BoiledEgg();
    }

    @Test
    public void testBoiledEggIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(boiledEggClass.getModifiers()));
    }

    @Test
    public void testBoiledEggIsATopping() {
        Collection<Type> interfaces = Arrays.asList(boiledEggClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping")));
    }

    @Test
    public void testBoiledEggOverrideGetNameMethod() throws Exception {
        Method getName = boiledEggClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    //buat test untuk menguji hasil dari pemanggilan method
    @Test
    public void testBoiledEggOverrideGetDescriptionMethodReturn() throws Exception {
        assertNotEquals(null, boiledEgg.getDescription());
    }
}