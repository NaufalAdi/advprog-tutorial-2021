package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

//add tests
public class CheeseTest {
    private Class<?> cheeseClass;
    private Cheese cheese;

    @BeforeEach
    public void setUp() throws Exception {
        cheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese");
        cheese = new Cheese();
    }

    @Test
    public void testCheeseIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(cheeseClass.getModifiers()));
    }

    @Test
    public void testCheeseIsATopping() {
        Collection<Type> interfaces = Arrays.asList(cheeseClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping")));
    }

    @Test
    public void testCheeseOverrideGetNameMethod() throws Exception {
        Method getName = cheeseClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    //buat test untuk menguji hasil dari pemanggilan method
    @Test
    public void testCheeseOverrideGetDescriptionMethodReturn() throws Exception {
        assertNotEquals(null, cheese.getDescription());
    }
}