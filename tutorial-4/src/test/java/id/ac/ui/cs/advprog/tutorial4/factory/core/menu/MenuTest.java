package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MenuTest {
    private Menu menu;

    @BeforeEach
    public void setUp() {
        menu = new InuzumaRamen("RamenTest");
    }

    @Test
    public void testMenuGetNameReturnValue() throws Exception {
        assertEquals("RamenTest", menu.getName());
    }

    @Test
    public void testMenuGetNoodleReturnValue() throws Exception {
        assertNotNull(menu.getNoodle());
        assertTrue(menu.getNoodle() instanceof Ramen);
    }

    @Test
    public void testMenuGetMeatReturnValue() throws Exception {
        assertNotNull(menu.getMeat());
        assertTrue(menu.getMeat() instanceof Pork);
    }

    @Test
    public void testMenuGetToppingReturnValue() throws Exception {
        assertNotNull(menu.getTopping());
        assertTrue(menu.getTopping() instanceof BoiledEgg);
    }

    @Test
    public void testMenuGetFlavorReturnValue() throws Exception {
        assertNotNull(menu.getFlavor());
        assertTrue(menu.getFlavor() instanceof Spicy);

    }

    @Test
    public void testInuzumaRamenHasBeenConstructed() throws Exception {
        InuzumaRamen inazumaRamen = new InuzumaRamen("DummyRamen");
        assertNotNull(inazumaRamen);
        assertTrue(inazumaRamen.getNoodle() instanceof Ramen);
        assertTrue(inazumaRamen.getMeat() instanceof Pork);
        assertTrue(inazumaRamen.getTopping() instanceof BoiledEgg);
        assertTrue(inazumaRamen.getFlavor() instanceof Spicy);

    }

    @Test
    public void testLiyuanSobaHasBeenConstructed() throws Exception {
        LiyuanSoba liyuanSoba = new LiyuanSoba("DummySoba");
        assertNotNull(liyuanSoba);
        assertTrue(liyuanSoba.getNoodle() instanceof Soba);
        assertTrue(liyuanSoba.getMeat() instanceof Beef);
        assertTrue(liyuanSoba.getTopping() instanceof Mushroom);
        assertTrue(liyuanSoba.getFlavor() instanceof Sweet);
    }

    @Test
    public void testMondoUdonHasBeenConstructed() throws Exception {
        MondoUdon mondoUdon = new MondoUdon("DummyUdon");
        assertNotNull(mondoUdon);
        assertTrue(mondoUdon.getNoodle() instanceof Udon);
        assertTrue(mondoUdon.getMeat() instanceof Chicken);
        assertTrue(mondoUdon.getTopping() instanceof Cheese);
        assertTrue(mondoUdon.getFlavor() instanceof Salty);
    }

    @Test
    public void testSnevnezhaShiratakiHasBeenConstructed() throws Exception {
        SnevnezhaShirataki snevnezhaShirataki = new SnevnezhaShirataki("DummyShirataki");
        assertNotNull(snevnezhaShirataki);
        assertTrue(snevnezhaShirataki.getNoodle() instanceof Shirataki);
        assertTrue(snevnezhaShirataki.getMeat() instanceof Fish);
        assertTrue(snevnezhaShirataki.getTopping() instanceof Flower);
        assertTrue(snevnezhaShirataki.getFlavor() instanceof Umami);
    }
}

