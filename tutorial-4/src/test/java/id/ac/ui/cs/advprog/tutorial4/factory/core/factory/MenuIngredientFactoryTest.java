package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuIngredientFactoryTest {
    private Class<?> menuIngredientFactoryClass;

    @BeforeEach
    public void setup() throws Exception {
        menuIngredientFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuIngredientFactory");
    }

    @Test
    public void testMenuIngredientFactoryIsAPublicInterface() {
        int classModifiers = menuIngredientFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testMenuIngredientFactoryHasCreateFlavorAbstractMethod() throws Exception {
        Method getDescription = menuIngredientFactoryClass.getDeclaredMethod("createFlavor");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }

    @Test
    public void testMenuIngredientFactoryHasCreateMeatAbstractMethod() throws Exception {
        Method getDescription = menuIngredientFactoryClass.getDeclaredMethod("createMeat");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }

    @Test
    public void testMenuIngredientFactoryHasCreateNoodleAbstractMethod() throws Exception {
        Method getDescription = menuIngredientFactoryClass.getDeclaredMethod("createNoodle");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }

    @Test
    public void testMenuIngredientFactoryHasCreateToppingAbstractMethod() throws Exception {
        Method getDescription = menuIngredientFactoryClass.getDeclaredMethod("createTopping");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }
}