package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

//add tests
public class SaltyTest {
    private Class<?> saltyClass;
    private Salty salty;

    @BeforeEach
    public void setUp() throws Exception {
        saltyClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty");
        salty = new Salty();
    }

    @Test
    public void testSaltyIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(saltyClass.getModifiers()));
    }

    @Test
    public void testSaltyIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(saltyClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor")));
    }

    @Test
    public void testSaltyOverrideGetNameMethod() throws Exception {
        Method getName = saltyClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    //buat test untuk menguji hasil dari pemanggilan method
    @Test
    public void testSaltyOverrideGetDescriptionMethodReturn() throws Exception {
        assertNotEquals(null, salty.getDescription());
    }
}