package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

//add tests
public class UdonTest {
    private Class<?> udonClass;
    private Udon udon;

    @BeforeEach
    public void setUp() throws Exception {
        udonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon");
        udon = new Udon();
    }

    @Test
    public void testUdonIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(udonClass.getModifiers()));
    }

    @Test
    public void testUdonIsANoodle() {
        Collection<Type> interfaces = Arrays.asList(udonClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle")));
    }

    @Test
    public void testUdonOverrideGetNameMethod() throws Exception {
        Method getName = udonClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    //buat test untuk menguji hasil dari pemanggilan method
    @Test
    public void testUdonOverrideGetDescriptionMethodReturn() throws Exception {
        assertNotEquals(null, udon.getDescription());
    }
}