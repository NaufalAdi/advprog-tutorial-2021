package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

//add tests
public class PorkTest {
    private Class<?> porkClass;
    private Pork pork;

    @BeforeEach
    public void setUp() throws Exception {
        porkClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork");
        pork = new Pork();
    }

    @Test
    public void testPorkIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(porkClass.getModifiers()));
    }

    @Test
    public void testPorkIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(porkClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat")));
    }

    @Test
    public void testPorkOverrideGetNameMethod() throws Exception {
        Method getName = porkClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    //buat test untuk menguji hasil dari pemanggilan method
    @Test
    public void testPorkOverrideGetDescriptionMethodReturn() throws Exception {
        assertNotEquals(null, pork.getDescription());
    }
}