package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

//add tests
public class SobaTest {
    private Class<?> sobaClass;
    private Soba soba;

    @BeforeEach
    public void setUp() throws Exception {
        sobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba");
        soba = new Soba();
    }

    @Test
    public void testSobaIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(sobaClass.getModifiers()));
    }

    @Test
    public void testSobaIsANoodle() {
        Collection<Type> interfaces = Arrays.asList(sobaClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle")));
    }

    @Test
    public void testSobaOverrideGetNameMethod() throws Exception {
        Method getName = sobaClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    //buat test untuk menguji hasil dari pemanggilan method
    @Test
    public void testSobaOverrideGetDescriptionMethodReturn() throws Exception {
        assertNotEquals(null, soba.getDescription());
    }
}