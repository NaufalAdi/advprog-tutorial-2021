package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
//import org.mockito.junit.MockitoJ
import static org.junit.jupiter.api.Assertions.*;


public class OrderServiceTest {
    private OrderServiceImpl orderService;

    @BeforeEach
    public void setUp() {
        orderService = new OrderServiceImpl();
    }

    @Test
    public void testOrderImplGetDrinkReturnValue() throws Exception {
        assertNotNull(orderService.getDrink());
    }

    @Test
    public void testOrderImplGetFoodReturnValue() throws Exception {
        assertNotNull(orderService.getFood());
    }

    @Test
    public void testOrderImplOrderADrink() throws Exception {
        orderService.orderADrink("Susu");
        assertEquals("Susu", orderService.getDrink().getDrink());
    }

    @Test
    public void testOrderImplOrderAFood() throws Exception {
        orderService.orderAFood("Telor");
        assertEquals("Telor", orderService.getFood().getFood());
    }




}
