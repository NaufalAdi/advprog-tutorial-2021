package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuServiceTest {
    MenuServiceImpl menuService;
    MenuServiceImpl menuServiceParam;

    @BeforeEach
    public void setUp() {
        menuService = new MenuServiceImpl();
        menuServiceParam = new MenuServiceImpl(new MenuRepository());
    }

    @Test
    public void testMenuImplCreateMenu() throws Exception {
        menuService.createMenu("LiyuanMenu", "LiyuanSoba");
        menuService.createMenu("InuzumaMenu", "InuzumaRamen");
        menuService.createMenu("MondoUdonMenu", "MondoUdon");
        menuService.createMenu("SnevnezhaShiratakiMenu", "SnevnezhaShirataki");
    }


    @Test
    public void testMenuImplGetMenusReturnValue() throws Exception {
        assertNotNull(menuService.getMenus());
        assertTrue(menuService.getMenus() instanceof ArrayList);
    }




}
