package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

//add tests
public class ChickenTest {
    private Class<?> chickenClass;
    private Chicken chicken;

    @BeforeEach
    public void setUp() throws Exception {
        chickenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken");
        chicken = new Chicken();
    }

    @Test
    public void testChickenIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(chickenClass.getModifiers()));
    }

    @Test
    public void testChickenIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(chickenClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat")));
    }

    @Test
    public void testChickenOverrideGetNameMethod() throws Exception {
        Method getName = chickenClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    //buat test untuk menguji hasil dari pemanggilan method
    @Test
    public void testChickenOverrideGetDescriptionMethodReturn() throws Exception {
        assertNotEquals(null, chicken.getDescription());
    }
}