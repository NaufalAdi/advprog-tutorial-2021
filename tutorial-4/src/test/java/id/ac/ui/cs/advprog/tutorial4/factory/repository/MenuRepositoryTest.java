package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;


@Repository
public class MenuRepositoryTest {

    private MenuRepository menuRepository;

    @BeforeEach
    public void setUp() {
        menuRepository = new MenuRepository();
    }

    @Test
    public void testMenuRepositoryGetMenuReturnValue() throws Exception {
        assertNotNull(menuRepository.getMenus());
        assertTrue(menuRepository.getMenus() instanceof ArrayList);
    }

    @Test
    public void testMenuRepositoryAddMethod() throws Exception {
        menuRepository.add(new SnevnezhaShirataki("DummyShirataki"));
        assertEquals(1, (menuRepository.getMenus().size()));
    }
}

