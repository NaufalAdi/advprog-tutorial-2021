package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;

public class Spell {
    private String text;
    private Codex codex;

    public Spell(String text, Codex codex){
        this.text = text; // string yg mau di ubah
        this.codex = codex; // bahasa asal
    }

    public String getText(){
        return text;
    }

    public Codex getCodex(){
        return codex;
    }
}
