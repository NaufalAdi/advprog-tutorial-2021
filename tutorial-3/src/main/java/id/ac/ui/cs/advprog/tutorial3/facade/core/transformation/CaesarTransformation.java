package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarTransformation {

    private int key;

    public CaesarTransformation() {
        this.key = 13;
    }

    public CaesarTransformation(int key){
        this.key = key;
    }

    public Spell encode(Spell spell) {
        return process(spell, true);
    }

    public Spell decode(Spell spell) {
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode) {
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int selector = encode ? 1 : -1;
        int n = text.length();
        StringBuilder res = new StringBuilder();
        for (char c : text.toCharArray()) {
            if (Character.isUpperCase(c)) {
                c = (char) ((c - 'A' + (selector * key)) % 26 + 'A');
                c = c < 'A' ? (char)(c + 26) : c;
            } else if (Character.isLowerCase(c)){
                c = (char) ((c - 'a' + (selector * key)) % 26 + 'a');
                c = c < 'a' ? (char)(c + 26) : c;
            }
            res.append(c);
        }
        return new Spell(res.toString(), codex);
    }

}
