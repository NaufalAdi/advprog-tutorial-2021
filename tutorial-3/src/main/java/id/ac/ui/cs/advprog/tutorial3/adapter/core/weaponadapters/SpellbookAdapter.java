package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private Boolean canUseLargeSpell;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.canUseLargeSpell = true;
    }

    @Override
    public String normalAttack() {
        canUseLargeSpell = true;
        return spellbook.smallSpell() ;
    }

    @Override
    public String chargedAttack() {
        if (canUseLargeSpell) {
            canUseLargeSpell = false;
            return spellbook.largeSpell();
        } else {
            return "Large Spell isn't ready";
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
