package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    private boolean adapterExists = false;

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        if (!adapterExists) {
            this.adapterExists = true;
            for (Bow bow : bowRepository.findAll()) {
                weaponRepository.save(new BowAdapter(bow));
            }
            for (Spellbook sb : spellbookRepository.findAll()) {
                weaponRepository.save(new SpellbookAdapter(sb));
            }
        }
        return weaponRepository.findAll();
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        String attackDesc;
        String strAttackType;
        if (attackType == 0) {
            attackDesc = weapon.normalAttack();
            strAttackType = " (normal attack): ";
        } else {
            attackDesc = weapon.chargedAttack();
            strAttackType = " (charged attack): ";
        }
        logRepository.addLog(weapon.getHolderName() + " attacked with " + weapon.getName() +
                 strAttackType + attackDesc);
    weaponRepository.save(weapon);
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
