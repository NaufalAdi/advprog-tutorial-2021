package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CaesarTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;

public class CipherTool {
    private final CelestialTransformation celestialTransformation;
    private final AbyssalTransformation abyssalTransformation;
    private final CaesarTransformation caesarTransformation;

    public CipherTool() {
        celestialTransformation = new CelestialTransformation();
        abyssalTransformation = new AbyssalTransformation();
        caesarTransformation = new CaesarTransformation(4);
    }
    
    public String encode(String text){
        Spell spell = new Spell(text, AlphaCodex.getInstance());
        spell = abyssalTransformation.encode(spell);
        spell = celestialTransformation.encode(spell);
        spell = caesarTransformation.encode(spell);
        spell = CodexTranslator.translate(spell, RunicCodex.getInstance());
        return spell.getText();
    }

    public String decode(String code){
        Spell spell = new Spell(code, RunicCodex.getInstance());
        spell = CodexTranslator.translate(spell, AlphaCodex.getInstance());
        spell = caesarTransformation.decode(spell);
        spell = celestialTransformation.decode(spell);
        spell = abyssalTransformation.decode(spell);

        return spell.getText();
    }
}
