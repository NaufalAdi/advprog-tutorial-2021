package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class CipherToolTest {
    private Class<?> CipherClass;
    private CipherTool Cipher;

    @BeforeEach
    public void setup() throws Exception {
        CipherClass = CipherTool.class;
        Cipher = new CipherTool();
    }

    @Test
    public void testCipherHasEncodeMethod() throws Exception {
        Method encode = CipherClass.getDeclaredMethod("encode", String.class);
        int methodModifiers = encode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCipherEncodeResult() {
        String expected = "(@.q:+cz!,";
        assertEquals(expected, Cipher.encode("Halo 12345"));
    }

    @Test
    public void testCipherHasDecodeMethod() throws Exception {
        Method decode = CipherClass.getDeclaredMethod("decode", String.class);
        int methodModifiers = decode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCipherDecodeResult() {
        String expected = "Halo 12345";
        assertEquals(expected, Cipher.decode("(@.q:+cz!,"));
    }

}

