package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpiritState;

public abstract class HighSpiritSpell implements Spell {
	protected HighSpirit spirit;

    public HighSpiritSpell(HighSpirit spirit) {
        this.spirit = spirit;
    }

    @Override
    public void undo() {
        //List<String> lifeArchive = spirit.getLifeArchive();
        //.remove(lifeArchive.size()-1);
        HighSpiritState prevState = spirit.getPrevState();
        if (prevState == HighSpiritState.ATTACK) spirit.attackStance();
        else if (prevState == HighSpiritState.DEFEND) spirit.defenseStance();
        else if (prevState == HighSpiritState.STEALTH) spirit.stealthStance();
        else if (prevState == HighSpiritState.SEALED) spirit.seal();
    }
}
