# Database Table

### Mahasiswa
|<ins>npm</ins>|email|ipk|nama|no_telp|kode_matkul
| ------------ |:---:|:-----:|:-----:|:-----:| -----:|

### Mata Kuliah

|<ins>kode_matkul</ins>|nama_matkul|prodi
|----------------------|:---------:| -----:|

### Log

|<ins>id</ins>|deskripsi|start_time|end_time|npm
| ----------- |:-------:|:--------:|:------:| ---:|

# To-Do List

## Mahasiswa
- Create mahasiswa
- Return mahasiswa
- Update mahasiswa
- Delete mahasiswa
- Daftar mata kuliah  
- Return semua mahasiswa

## Mata Kuliah
- Create mata kuliah
- Return mata kuliah
- Update mata kuliah
- Delete mata kuliah
- Return semua mata kuliah
- Return asdos

## Log & Summary
- Create log
- Return semua log suatu mahasiswa
- Return log suatu mahasiswa dalam bulan tertentu  
- Delete log
- Summary bulan tertentu
- Summary semua bulan


    
