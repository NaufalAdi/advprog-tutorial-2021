package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.Summary;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;

    @Mock
    private MahasiswaService mahasiswaService;

    @InjectMocks
    private LogServiceImpl logService;

    private Mahasiswa mahasiswa;
    private Log log;
    private Log log2;
    private Summary summary;
    private Summary summary2;

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1906192052");
        mahasiswa.setNama("Maung Meong");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");
        log = new Log();
        log.setId(1);
        log.setStart(LocalDateTime.parse("2018-05-10T17:45:55.9483536"));
        log.setEnd(LocalDateTime.parse("2018-05-11T21:45:55.9483536"));
        log.setMahasiswa(mahasiswa);
        log.setDeskripsi("test");
        log2 = new Log();
        log2.setId(2);
        log2.setStart(LocalDateTime.parse("2018-06-10T16:45:55.9483536"));
        log2.setEnd(LocalDateTime.parse("2018-06-11T17:45:55.9483536"));
        log2.setMahasiswa(mahasiswa);
        log2.setDeskripsi("logdua");
        summary = new Summary(
                "May",
                log.getJamKerja()
        );

        summary2 = new Summary(
                "June",
                log2.getJamKerja()
        );


    }

    @Test
    public void testServiceCreateLog(){
        lenient().when(logService.createLog("1906192052", log)).thenReturn(log);
    }

    @Test
    public void testServiceGetLogById(){
        lenient().when(logService.getLogById("1")).thenReturn(log);
        Log resultLog = logService.getLogById("1");
        assertEquals(log.getDeskripsi(), resultLog.getDeskripsi());
    }

    @Test
    public void testServiceDeleteLogById(){
        logService.createLog("1906192052", log);
        logService.deleteLogById("1");
        lenient().when(logService.getLogById("1")).thenReturn(null);
        assertEquals(null, logService.getLogById(Integer.toString(log.getId())));
    }

    @Test
    public void testServiceUpdateLogById() {
        logService.createLog("1906192052", log);
        String currentDeskripsi = log.getDeskripsi();
        //Change Deskripsi frim test to blablabla
        log.setDeskripsi("blablabla");

        lenient().when(logService.updateLogById(Integer.toString(log.getId()), log)).thenReturn(log);
        Log resultLog = logService.updateLogById(Integer.toString(log.getId()), log);

        assertNotEquals(resultLog.getDeskripsi(), currentDeskripsi);
        assertEquals(resultLog.getStart(), log.getStart());
    }

    @Test
    public void testServiceGetListLogByNPM(){
        List<Log> allLog = logRepository.findByMahasiswa(mahasiswa);
        lenient().when(logService.getListLogByNPM(mahasiswa.getNpm())).thenReturn(allLog);
        Iterable<Log> allLogResult = logService.getListLogByNPM("1906192052");
        Assertions.assertIterableEquals(allLog, allLogResult);

    }

    @Test
    public void testGetGetListLogByNPMMonth() {
        List<Log> logList = new LinkedList<>();
        logList.add(log);
        logList.add(log2);
        when(logRepository.findByMahasiswa(mahasiswa)).thenReturn(logList);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        Iterable<Log> result = logService.getListLogByNPMMonth(mahasiswa.getNpm(), "May");
        logList.remove(log2);
        assertEquals(logList, result);

    }
    @Test
    public void testGetSummaryByNPMMonth(){
        List<Log> logList = new LinkedList<>();
        logList.add(log);
        logList.add(log2);
        when(logRepository.findByMahasiswa(mahasiswa)).thenReturn(logList);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        Summary result = logService.getSummary(mahasiswa.getNpm(), "May");
        assertEquals(summary, result);
    }

    @Test
    public void testGetSummaryByNPM(){
        List<Log> logList = new LinkedList<>();
        logList.add(log);
        logList.add(log2);
        List<Summary> expected = new LinkedList<>();
        expected.add(summary);
        expected.add(summary2);
        when(logRepository.findByMahasiswa(mahasiswa)).thenReturn(logList);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        Iterable<Summary> result = logService.getAllSummary(mahasiswa.getNpm());
        assertEquals(expected, result);
    }
/*
    @Test
    public void testServiceGetListMahasiswa(){
        Iterable<Mahasiswa> listMahasiswa = mahasiswaRepository.findAll();
        lenient().when(mahasiswaService.getListMahasiswa()).thenReturn(listMahasiswa);
        Iterable<Mahasiswa> listMahasiswaResult = mahasiswaService.getListMahasiswa();
        Assertions.assertIterableEquals(listMahasiswa, listMahasiswaResult);
    }

    @Test
    public void testServiceGetMahasiswaByNpm(){
        lenient().when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        Mahasiswa resultMahasiswa = mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm());
        assertEquals(mahasiswa.getNpm(), resultMahasiswa.getNpm());
    }




    }
*/
}
