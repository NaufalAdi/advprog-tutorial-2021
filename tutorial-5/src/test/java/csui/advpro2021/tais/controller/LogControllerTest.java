package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.*;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.service.*;
import java.util.*;
import java.text.*;
import java.time.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LogController.class)
public class LogControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogService logService;

    @MockBean
    private LogRepository logRepository;

    @MockBean
    private MahasiswaService mahasiswaService;

    @MockBean
    private MataKuliahService mataKuliahService;

    private Log log;
    private Mahasiswa mahasiswa;
    private MataKuliah mataKuliah;
    private Summary summary;

    @BeforeEach
    public void setUp() throws ParseException {
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1906192052");
        mahasiswa.setNama("Maung Meong");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");

        log = new Log();
        log.setId(1);
        log.setStart(LocalDateTime.parse("2018-05-10T17:45:55.9483536"));
        log.setEnd(LocalDateTime.parse("2018-05-11T21:45:55.9483536"));
        log.setDeskripsi("test");
        log.setMahasiswa(mahasiswa);

        mataKuliah = new MataKuliah();
        mataKuliah.setKodeMatkul("ADVPROG");
        mataKuliah.setNama("Advanced Programming");
        mataKuliah.setProdi("Ilmu Komputer");
        mataKuliah.setMahasiswas(new ArrayList<>());

        mahasiswa.setMataKuliah(mataKuliah);

        summary = new Summary(
                "May",
                log.getJamKerja()
        );
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return objectMapper.writeValueAsString(obj);

    }

    private String mapLogToJson(Log log) throws JsonProcessingException {
        return String.format("{\"start_time\": \"2018-05-10T17:45:55.9483536\", \"end_time\": \"2018-05-11T21:45:55.9483536\", \"deskripsi\": \"%s\"}", log.getDeskripsi());
    }

    @Test
    public void testControllerCreateLog() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(logService.createLog(mahasiswa.getNpm(), log)).thenReturn(log);
        mvc.perform(post("/log/mahasiswa/1906192052")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapLogToJson(log)));
    }

    @Test
    public void testControllerDeleteLog() throws Exception {
        mvc.perform(delete("/log/id/" + log.getId()))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testControllerUpdateLog() throws Exception {
        when(logService.getLogById(Integer.toString(log.getId()))).thenReturn(log);

        Log newLog = new Log("2020-05-10T17:45:55.00", "2020-05-11T21:45:55.00", "baru");
        newLog.setMahasiswa(mahasiswa);

        when(logService.updateLogById(anyString(),  any(Log.class))).thenReturn(newLog);

        String str = "{\"start_time\": \"2020-05-10T17:45:55.00\", \"end_time\": \"2020-05-11T21:45:55.00\", \"deskripsi\": \"baru\"}";

        mvc.perform(put("/log/id/" + log.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(str))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deskripsi").value("baru"));
    }

    @Test
    public void testControllerGetLogById() throws Exception {
        when(logService.getLogById("1")).thenReturn(log);
        mvc.perform(get("/log/id/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value("1"));
    }


    @Test
    public void testControllerGetLogsMahasiswa() throws Exception {
        Iterable<Log> logs = Arrays.asList(log);
        when(logService.getListLogByNPM("1906192052")).thenReturn(logs);

        mvc.perform(get("/log/mahasiswa/1906192052").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(1));
    }

    @Test
    public void testControllerGetLogsMahasiswaPerMonth() throws Exception {
        Iterable<Log> logs = Arrays.asList(log);
        when(logService.getListLogByNPMMonth(anyString(),anyString())).thenReturn(logs);

        mvc.perform(get("/log/mahasiswa/1906192052?month=5").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(1));
    }

    @Test
    public void testControllerGetLogsReportMahasiswa() throws Exception {
        Iterable<Summary> summaries = Arrays.asList(summary);
        when(logService.getAllSummary("1906192052")).thenReturn(summaries);
        mvc.perform(get("/log/mahasiswa/1906192052/summary").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].month").value("May"))
                .andExpect(jsonPath("$[0].pembayaran").value("9800"));
    }

    @Test
    public void testControllerGetLogsReportMahasiswaByMonth() throws Exception {
        when(logService.getSummary("1906192052","May")).thenReturn(summary);
        mvc.perform(get("/log/mahasiswa/1906192052/summary?month=May").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.month").value("May"))
                .andExpect(jsonPath("$.pembayaran").value("9800"));
    }
}
