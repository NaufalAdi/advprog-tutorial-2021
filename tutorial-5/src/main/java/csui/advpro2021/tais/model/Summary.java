package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class Summary {
    @JsonIgnore
    private final long payRate = 350;

    private String month;

    private double jamKerja;

    private long pembayaran;

    public Summary(String month, double jamKerja){
        this.month = month;
        this.jamKerja = jamKerja;
        this.pembayaran = (long) (payRate * jamKerja);
    }
}
