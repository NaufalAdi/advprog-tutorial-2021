package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@Entity
@Table(name = "Log")
@Data
@NoArgsConstructor
public class Log {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(name="deskripsi")
    private String deskripsi;

    @Column(name="start_time")
    private LocalDateTime start;

    @Column(name="end_time")
    private LocalDateTime end;

    @ManyToOne
    @JoinColumn(name="npm")
    private Mahasiswa mahasiswa;
/*
    public Log(String start, String end, String deskripsi) {
        this.mahasiswa = null;
        this.start = LocalDateTime.parse(start);
        this.end = LocalDateTime.parse(end);
        this.deskripsi = deskripsi;
    }
*/


    @JsonCreator
    public Log(
            @JsonProperty("start_time") String start,
            @JsonProperty("end_time") String end,
            @JsonProperty("deskripsi") String deskripsi
    ) {
        this.start = LocalDateTime.parse(start);
        this.end = LocalDateTime.parse(end);
        this.deskripsi = deskripsi;
    }

    public double getJamKerja(){
        return (this.start.until(this.end, ChronoUnit.SECONDS))/3600.0;
    }
}
