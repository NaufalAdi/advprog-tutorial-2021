package csui.advpro2021.tais.service;


import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.Summary;
import csui.advpro2021.tais.repository.LogRepository;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class LogServiceImpl implements LogService{
    @Autowired
    private LogRepository logRepository;

    @Autowired
    private MahasiswaService mahasiswaService;

    @Override
    public Log createLog(String npm, Log log) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        log.setMahasiswa(mahasiswa);
        logRepository.save(log);
        return log;
    }

    @Override
    public Log getLogById(String id) {
        return logRepository.findById(Integer.parseInt(id));
    }

    @Override
    public Log updateLogById(String id, Log log) {
        log.setId(Integer.parseInt(id));
        logRepository.save(log);
        return log;
    }

    @Override
    public void deleteLogById(String id) {
        logRepository.deleteById(Integer.parseInt(id));
    }

    @Override
    public Iterable<Log> getListLogByNPM(String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        List<Log> logs = logRepository.findByMahasiswa(mahasiswa);
        return logs;
    }

    @Override
    public Iterable<Log> getListLogByNPMMonth(String npm , String month) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        List<Log> logs = logRepository.findByMahasiswa(mahasiswa);
        List<Log> result = new LinkedList<>();
        if (month != null) {
            for (Log log : logs) {
                if (month.equalsIgnoreCase((log.getStart()).getMonth().name())) {
                    result.add(log);
                }
            }
        }
        return result;
    }

    @Override
    public Summary getSummary(String npm, String month) {
        month = month.substring(0, 1).toUpperCase() + month.substring(1);
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        List<Log> allLog = logRepository.findByMahasiswa(mahasiswa);
        int totalJamKerja = 0;
        for(Log log : allLog){
            System.out.println(log.getDeskripsi());
            if(month.equalsIgnoreCase((log.getStart()).getMonth().name())){
                totalJamKerja += log.getJamKerja();
            }
        }
        return new Summary(month, totalJamKerja);
    }

    @Override
    public Iterable<Summary> getAllSummary(String npm) {
        String[] months = {"January","February","March","April","May","June","July","August","September","October","November","December"};
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        List<Log> allLog = logRepository.findByMahasiswa(mahasiswa);
        List<Summary> allSummary = new LinkedList<>();
        for (String month : months) {
            int totalJamKerja = 0;
            for (Log log : allLog) {
                if (month.equalsIgnoreCase((log.getStart()).getMonth().name())) {
                    totalJamKerja += log.getJamKerja();
                }
            }
            System.out.println(totalJamKerja);
            if(totalJamKerja !=0) allSummary.add(new Summary(month, totalJamKerja));
        }
        return allSummary;
    }
}
