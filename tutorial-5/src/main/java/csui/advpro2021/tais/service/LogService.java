package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Summary;

public interface LogService {
    Log createLog(String npm, Log log);
    Log getLogById(String id);
    Log updateLogById(String id, Log log);
    void deleteLogById(String id);
    Iterable<Log> getListLogByNPM(String npm);
    Iterable<Log> getListLogByNPMMonth(String npm , String month);
    Summary getSummary(String npm, String month);
    Iterable<Summary> getAllSummary(String npm);
}
