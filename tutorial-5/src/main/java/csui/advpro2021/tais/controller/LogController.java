package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.service.LogService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/log")
public class LogController {
    @Autowired
    private LogService logService;

    @PostMapping(path = "/mahasiswa/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity postLog(@PathVariable(value = "npm") String npm, @RequestBody Log log) {
        return ResponseEntity.ok(logService.createLog(npm, log));
    }

    @GetMapping(path = "/id/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLogById(@PathVariable(value = "id") String id) {
        return ResponseEntity.ok(logService.getLogById(id));
    }

    @PutMapping(path = "/id/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateLog(@RequestBody Log log, @PathVariable(value = "id") String id) {
        return ResponseEntity.ok(logService.updateLogById(id, log));
    }

    @DeleteMapping(path = "/id/{id}", produces = {"application/json"})
    public ResponseEntity deleteLog(@PathVariable(value = "id") String id) {
        logService.deleteLogById(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "/mahasiswa/{npm}", produces = {"application/json"})
    public ResponseEntity getLogMahasiswa(@PathVariable(value = "npm") String npm, @RequestParam(required = false) String month) {
        if (month == null) return ResponseEntity.ok(logService.getListLogByNPM(npm));
        return ResponseEntity.ok(logService.getListLogByNPMMonth(npm, month));
    }

    @GetMapping(path = "/mahasiswa/{npm}/summary", produces = {"application/json"})
    public ResponseEntity getSummary(@PathVariable(value = "npm") String npm, @RequestParam(required = false) String month){
        if (month == null) return ResponseEntity.ok(logService.getAllSummary(npm));
        return ResponseEntity.ok(logService.getSummary(npm, month));
    }


}
